import { Injectable } from '@angular/core';
import { User } from '../interfaces/user';
import { of, Observable } from 'rxjs';
import { HttpClient,  HttpHeaders } from '@angular/common/http';
import { catchError, tap,  } from 'rxjs/operators';
import { Subject } from 'rxjs';


const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class HttpLoginService {
  private apiUrl = 'http://localhost:8000/api/';
  public currentUser: Subject<User> = new Subject<User>();
  public userToken: Subject<string> = new Subject<string>();

  constructor(
    private http: HttpClient
  ) { }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any) => {
        console.error('qqqqqq', error);
        return of(error as T);
    };
  }
  public getCurrentUser(): User {
    return JSON.parse(localStorage.getItem('currentUser'));
  }  

  public signIn(emailPassword: User): Observable<User | any> {
    return this.http.post<User| any>(this.apiUrl + 'signin', JSON.stringify(emailPassword), httpOptions).pipe(
      tap(user => {
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUser.next(user);
        this.userToken.next(user.token);
      }));
  }


  public signUp(emailPassword: User): Observable<User | any> {
    return this.http.post<User| any>(this.apiUrl + 'signup', JSON.stringify(emailPassword), httpOptions).pipe(
      tap(user => {
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUser.next(user);
        this.userToken.next(user.token);
      }));
  }

  public signOut(): void {
    localStorage.removeItem('currentUser');
    this.currentUser.next(null);
    this.userToken.next('');
  }
}
