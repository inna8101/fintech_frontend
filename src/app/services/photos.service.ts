import {Injectable} from '@angular/core';
import {switchMap, map} from 'rxjs/operators';
import { HttpService } from './http';

@Injectable({
  providedIn: 'root'
})
export class PhotosService {
  constructor(private http: HttpService) {}

  getPhotosByCategory(category: string) {
    return this.http.getPhotosByCategory(category);
  }

  getPhotosByTags(tags: string) {
    return this.http.getPhotosByTags(tags);
  }

  getPhoto(id: string) {
    return this.http.getPhoto(id).pipe(
      switchMap(photo =>
        this.http.getUser(photo.user_id).pipe(
          map(owner => ({
            photo,
            owner,
          })),
        )
      )
    );
  }
}
