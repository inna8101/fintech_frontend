import { Injectable } from '@angular/core';
import { findWhere, without, find } from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class ModalService {
  private modals: any[] = [];

    add(modal: any) {
        this.modals.push(modal);
    }

    remove(id: string) {
        const modalToRemove = findWhere(this.modals, { id});
        this.modals = without(this.modals, modalToRemove);
    }

    open(id: string) {
        const modal = findWhere(this.modals, { id });
        modal.open();
    }

    close(id: string) {
        const modal = find(this.modals, { id });
        modal.close();
    }

  constructor() { }
}
