import { Photo } from '../interfaces/photo';
import { User } from '../interfaces/user';
import { Category } from '../interfaces/category';
import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';
import { HttpClient,  HttpHeaders } from '@angular/common/http';
import { catchError, map, tap,  } from 'rxjs/operators';
import { HttpLoginService } from './http-login.service';

let startToken = '';
const currentUserStr: string = localStorage.getItem('currentUser');

if (currentUserStr) {
  const currentUser = JSON.parse(currentUserStr);

  startToken = currentUser.token;
}

export function toFormData<T>( formValue: T ) {
  const formData = new FormData();

  for ( const key of Object.keys(formValue) ) {
    const value = formValue[key];
    formData.append(key, value);
  }
  return formData;
}

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  private apiUrl = 'http://localhost:8000/api/';

  private httpOptions = {
    headers: new HttpHeaders({'x-access-token': startToken})
  };

    constructor(
        private http: HttpClient,
        private httpLogin: HttpLoginService
    ) {
      this.httpLogin.userToken.subscribe(token => {
        this.httpOptions.headers = new HttpHeaders({'x-access-token': token});
      });
     }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any) => {
            console.error(error);
            return of(error as T);
        };
    }
    
    public getRandomImage(): Observable<string | any> {
        return this.http.get<string>(this.apiUrl + 'randomfile', this.httpOptions)
          .pipe(
            map(fileId => this.apiUrl + 'files/' + fileId),
            catchError(this.handleError('getRandomImage', []))
          );
    }

    public getCategories(): Observable<Category[]> {
      return this.http.get<Category[]>(this.apiUrl + 'categories', this.httpOptions)
        .pipe(
          catchError(this.handleError('getCategories', []))
        );
    }

    public postCategory(category: Category): Observable<Category | any> {
      return this.http.post<Category>(this.apiUrl + 'categories', category, this.httpOptions);
    }

    public deleteCategory(category: string): Observable<Category | any> {
      return this.http.delete<Category>(this.apiUrl + 'categories/' + category, this.httpOptions)
        .pipe(
          catchError(this.handleError('deleteCategory', []))
        );
    }

    public getPhotosByCategory(categoryId: string): Observable<Photo[]> {
        return this.http.get<Photo[]>(this.apiUrl + 'photos/?category=' + categoryId, this.httpOptions)
          .pipe(
            catchError(this.handleError('getPhotosByCategory', []))
          );
    }

    public getPhotosByTags(tags: string): Observable<Photo[]> {
      return this.http.get<Photo[]>(this.apiUrl + 'photos/?tags=' + tags, this.httpOptions)
        .pipe(
          catchError(this.handleError('getPhotosByTags', []))
        );
    }

    public getPhotosByUserName(userId: string): Observable<Photo[]> {
      return this.http.get<Photo[]>(this.apiUrl + 'photos/?userId=' + userId, this.httpOptions)
        .pipe(
          catchError(this.handleError('getPhotosByUser', []))
        );
    }

    public getPhoto(id: string): Observable<Photo | any> {
      return this.http.get<Photo>(this.apiUrl + 'photos/' + id, this.httpOptions)
        .pipe(
          catchError(this.handleError('getPhoto', []))
        );
    }

    public postPhoto(photo: Photo): Observable<Photo | any> {
      return this.http.post<Photo| any>(this.apiUrl + 'photos', toFormData(photo), this.httpOptions)
      .pipe(
        tap(_ => console.log(`POST photo=${photo.name}`)),
        catchError(this.handleError('postPhoto', []))
    );
    }

    public updatePhoto(photoId: string, newInfo: object): Observable<object | any> {
      return this.http.put<object>(this.apiUrl + 'photos/' + photoId, newInfo, this.httpOptions)
      .pipe(
        catchError(this.handleError('updatePhoto', []))
    );
    }
    public deletePhoto(photoId: string): Observable<object | any> {
      return this.http.delete<object>(this.apiUrl + 'photos/' + photoId, this.httpOptions)
      .pipe(
        catchError(this.handleError('deletePhoto', []))
    );
    }

    public getUser(id: string): Observable<User | any> {
      return this.http.get<User>(this.apiUrl + 'users/' + id, this.httpOptions)
        .pipe(
          catchError(this.handleError('getUser', []))
        );
    }

    public imprint(imprintInfo: object): Observable<object | any> {
      return this.http.post<object>(this.apiUrl + 'imprint', imprintInfo, this.httpOptions)
      .pipe(
        catchError(this.handleError('imprint', []))
    );
    }

}
