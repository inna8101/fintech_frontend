import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialAppModule } from './ngmaterial/ngmaterial.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms'; 
import { ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { HttpService } from './services/http';
import { HttpLoginService } from './services/http-login.service';
import {PhotosService} from './services/photos.service';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { RandomImagesComponent } from './components/random-images/random-images.component';
import { SigninComponent } from './components/signin/signin.component';
import { CategorySearchComponent } from './components/category-search/category-search.component';
import { PhotoCardComponent } from './components/photo-card/photo-card.component';
import { ProfileComponent } from './components/profile/profile.component';
import { UploadPhotoComponent } from './components/upload-photo/upload-photo.component';
import { TagsSearchComponent } from './components/tags-search/tags-search.component';
import { ShowPhotosComponent } from './components/show-photos/show-photos.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    RandomImagesComponent,
    SigninComponent,
    CategorySearchComponent,
    PhotoCardComponent,
    ProfileComponent,
    UploadPhotoComponent,
    TagsSearchComponent,
    ShowPhotosComponent,
    SignUpComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialAppModule,
    NgbModule,
    ReactiveFormsModule,
    MatCardModule,
    FormsModule
  ],
  providers: [
    HttpService,
    HttpLoginService,
    PhotosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
