import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SigninComponent } from './components/signin/signin.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { RandomImagesComponent } from './components/random-images/random-images.component';
import { CategorySearchComponent } from './components/category-search/category-search.component';
import { PhotoCardComponent } from './components/photo-card/photo-card.component';
import { TagsSearchComponent } from './components/tags-search/tags-search.component';
import { ProfileComponent } from './components/profile/profile.component';
import { UploadPhotoComponent } from './components/upload-photo/upload-photo.component';


const routes: Routes = [
  { path: 'mainpage', component: RandomImagesComponent },
  { path: '', redirectTo: '/mainpage', pathMatch: 'full' },
  { path: 'signin', component: SigninComponent },
  { path: 'signup', component: SignUpComponent },
  { path: 'upload', component: UploadPhotoComponent },
  { path: 'profile', component: ProfileComponent },
  { path: 'search', component: CategorySearchComponent },
  { path: 'card/:id', component: PhotoCardComponent },
  { path: 'tagssearch', component: TagsSearchComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
