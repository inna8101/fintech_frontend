import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../services/http';
import { HttpLoginService } from '../../services/http-login.service';
import {FormControl, FormGroup} from '@angular/forms';
import { Photo } from '../../interfaces/photo';
import { User } from '../../interfaces/user';
import { Category } from '../../interfaces/category';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  public currentUser: User;
  public userPhotos: Photo[] = [];
  public message = '';
  private category: Category = {
    name: ''
  };

  constructor(
    private http: HttpService,
    private httpLogin: HttpLoginService
  ) {
    this.httpLogin.currentUser.subscribe(currentUser => {
      this.currentUser = currentUser;
    });
  }

  ngOnInit() {
    this.currentUser = this.httpLogin.getCurrentUser();

    this.http.getPhotosByUserName(this.currentUser._id).subscribe(
      photos => {
      this.userPhotos = photos;
  });
  }

  onChange(categoryName) {
    this.category.name = categoryName;
  }
  addCategory(): void {
    this.http.postCategory(this.category).subscribe(
      category => {
        this.message = 'Category ' + category.name + ' added to category list';
      },
      error => {
        this.message = error.error.text;
      });
  }

  deleteCategory(): void {
    this.http.deleteCategory(this.category.name).subscribe(
      category => {
        if (!category) {
          this.message = 'This category doesnt exist!';
          return;
        }
        this.message =  'Category ' + category.name + ' deleted from category list';
      });
  }

}
