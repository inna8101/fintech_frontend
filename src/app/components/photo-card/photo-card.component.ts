import { Component, OnInit } from '@angular/core';
import { Photo } from '../../interfaces/photo';
import { User } from '../../interfaces/user';
import { HttpService } from '../../services/http';
import { HttpLoginService } from '../../services/http-login.service';
import { ActivatedRoute } from '@angular/router';
import * as dayjs from 'dayjs';
import { FormGroup, FormControl } from '@angular/forms';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Validators } from '@angular/forms';
import {tap} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {PhotosService} from 'src/app/services/photos.service';

@Component({
  selector: 'app-photo-card',
  templateUrl: './photo-card.component.html',
  styleUrls: ['./photo-card.component.css']
})
export class PhotoCardComponent implements OnInit {
  public photo: Photo;
  public date: string;
  public owner: User;
  public currentUser: User;
  public message: string;
  public photoInfo$: Observable<{photo: Photo, owner: User}>;

  imprintForm = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email])
  });

  constructor(
    private route: ActivatedRoute,
    private http: HttpService,
    private photosService: PhotosService,
    private httpLogin: HttpLoginService,
    private modalService: NgbModal
  ) {
    this.httpLogin.currentUser.subscribe(currentUser => {
      this.currentUser = currentUser;
    });
  }

  ngOnInit() {
    this.getPhoto();
    this.currentUser = this.httpLogin.getCurrentUser();
}
  getPhoto(): void {
    const id = this.route.snapshot.paramMap.get('id');

    this.photoInfo$ = this.photosService.getPhoto(id).pipe(
      tap(({photo, owner}) => {
        this.photo = photo;
        console.log('ttt', this.photo.tags);
        this.owner = owner;
        this.date = dayjs(photo.date).format('DD MMMM YYYY');
      }),
    );
  }

  toggleBanPhoto() {
    this.photo.isBanned = !this.photo.isBanned;

    this.http.updatePhoto( this.photo._id, { isBanned: this.photo.isBanned })
      .subscribe(response => {
      });
  }

  deletePhoto(): void {
    this.http.deletePhoto( this.photo._id)
      .subscribe(response => {
        this.message = response.message;
      });
  }

  showPhoto(): void {
    const answer = confirm('Are you over 18?');

    if (answer) {
      this.photo.isBanned = false;
    }
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'});
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.imprintForm.controls[controlName];
    const result = control.invalid && control.touched;

    return result;
  }

  onSubmit(modal): void {
    const imprintInfo = {
      recipient: this.owner,
      photo: this.photo,
      ...this.imprintForm.value
    };

    this.http.imprint(imprintInfo)
      .subscribe(response => {
        this.message = response.message;
      });
    this.imprintForm.reset();

    modal.close();
  }
}
