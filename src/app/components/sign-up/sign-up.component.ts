import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpLoginService } from '../../services/http-login.service';
import { Router } from '@angular/router';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  private repeatPassword = '';
  public message: string;

  signUpForm = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required)
  });

  constructor(
    private router: Router,
    private httpLogin: HttpLoginService
  ) { }

  ngOnInit() {
  }

  isControlInvalid(controlName: string): boolean {
    const control = this.signUpForm.controls[controlName];
    const result = control.invalid && control.touched;
    return result;
  }

  onChange(repeatPassword) {
    this.repeatPassword = repeatPassword;
}

  onSubmit() {
    this.message = '';
    if (this.repeatPassword !== this.signUpForm.value.password) {
      this.message = "Passwords don't match";
      return;
    }

    this.httpLogin.signUp(this.signUpForm.value).subscribe(
      user => {
        this.router.navigateByUrl('/profile');
      },
      error => {
        this.message = error.error.text;
      });
  }
  }


