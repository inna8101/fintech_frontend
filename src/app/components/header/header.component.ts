import { Component, OnInit } from '@angular/core';
import { HttpLoginService } from '../../services/http-login.service';
import { Router } from '@angular/router';
import { User } from 'src/app/interfaces/user';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public currentUser: User;

  constructor(
    private httpLogin: HttpLoginService,
    private router: Router
  ) {
    this.httpLogin.currentUser.subscribe(currentUser => {
      this.currentUser = currentUser;
    });
   }
  ngOnInit() {
    this.currentUser = this.httpLogin.getCurrentUser();
  }

  signOut() {
    this.httpLogin.signOut();
    this.router.navigateByUrl('/mainpage');
  }

}
