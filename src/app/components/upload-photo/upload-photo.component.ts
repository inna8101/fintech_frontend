import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';
import { Photo } from '../../interfaces/photo';
import { Category } from '../../interfaces/category';
import { User } from '../../interfaces/user';
import { HttpService } from '../../services/http';
import { HttpLoginService } from '../../services/http-login.service';

@Component({
  selector: 'app-upload-photo',
  templateUrl: './upload-photo.component.html',
  styleUrls: ['./upload-photo.component.css']
})
export class UploadPhotoComponent implements OnInit {
  public currentUser: User;
  public categories: Category[];
  public message: string;
  
  uploadForm = new FormGroup({
    photo: new FormControl('', Validators.required),
    category_id: new FormControl('', Validators.required),
    name: new FormControl('',  Validators.required),
    description: new FormControl(''),
    tags: new FormControl('')
  });

  constructor(
    private http: HttpService,
    private httpLogin: HttpLoginService,
    private cd: ChangeDetectorRef
    ) { }

  ngOnInit() {
    this.currentUser = this.httpLogin.getCurrentUser();
    this.http.getCategories().subscribe(
      categories => {
        this.categories = categories;
    });
  }

  onSubmit() {
    this.message = '';

    if (!this.currentUser) {
      return;
    }
    const photoForServer: Photo = {
      ...this.uploadForm.value,
      user_id: this.currentUser._id
    }

    this.http.postPhoto(photoForServer).subscribe(
      message => {
      this.message = message.message;
      this.uploadForm.reset();
    }); 
}

  isControlInvalid(controlName: string): boolean {
    const control = this.uploadForm.controls[controlName];
    const result = control.invalid && control.touched;
    return result;
    }

  onFileChange(event) {
    if (event.target.files && event.target.files.length) {
      const [photo] = event.target.files;
      this.uploadForm.patchValue({ photo });
      this.cd.markForCheck();
    }
  }

}
