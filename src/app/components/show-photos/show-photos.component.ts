import { Component, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { Input} from '@angular/core';
import { Router } from '@angular/router';
import { HttpLoginService } from '../../services/http-login.service';
import { HttpService } from '../../services/http';
import { Photo } from '../../interfaces/photo';
import { User } from '../../interfaces/user';
import { Message } from '../../interfaces/message';

@Component({
  selector: 'app-show-photos',
  templateUrl: './show-photos.component.html',
  styleUrls: ['./show-photos.component.css']
})
export class ShowPhotosComponent implements OnInit {
  public currentUser: User;
  @Input() photos: Photo[];
  public chooseClicked = false;
  private setPhotoIds = new Set<string>();
  private success: boolean[] = [];
  public message: string;

  constructor(
    private router: Router,
    private httpLogin: HttpLoginService,
    private http: HttpService
  ) {
      this.httpLogin.currentUser.subscribe(currentUser => {
        this.currentUser = currentUser;
    });
   }

  ngOnInit() {
    this.currentUser = this.httpLogin.getCurrentUser();
  }

  goToPhotoCard( photoId: string ): void {
    this.router.navigateByUrl('/card/' + photoId);
  }

  choosePhotos(): void {
    this.chooseClicked = true;
  }

  onChange( photoId: string ): void {
    if (this.setPhotoIds.has(photoId)) {
      this.setPhotoIds.delete(photoId);
      return;
    }
    this.setPhotoIds.add(photoId);
  }

  async banPhotos() {
    this.message = '';
    this.success = [];
    for (const photoId of this.setPhotoIds) {
      const message: Message = await this.http.updatePhoto( photoId, { isBanned: true }).toPromise();
      
      if (message.status === 'OK') {
        const index = this.photos.findIndex((photo): boolean => {
          if (photo._id === photoId) {
            return true;
          }
        });
        this.photos[index].isBanned = true;
        this.success.push(true);
      }
    }
    if (this.success.length === this.setPhotoIds.size ) {
      this.message = 'Selected photos were banned';
    }
  }

  async allowPhotos(){
    this.message = '';
    this.success = [];
    for (const photoId of this.setPhotoIds) {
      const message: Message = await this.http.updatePhoto( photoId, { isBanned: false }).toPromise();
      
      if (message.status === 'OK') {
        const index = this.photos.findIndex((photo): boolean => {
          if (photo._id === photoId) {
            return true;
          }
        });
        this.photos[index].isBanned = false;
        this.success.push(true);
      }
    }
    if (this.success.length === this.setPhotoIds.size ) {
      this.message = 'Selected photos were allowed';
    }
  }

  async deletePhotos(){
    this.message = '';
    this.success = [];
    for (const photoId of this.setPhotoIds) {
      const message = await this.http.deletePhoto( photoId).toPromise();
      
      if (message.status === 'OK') {
        const index = this.photos.findIndex((photo): boolean => {
          if (photo._id === photoId) {
            return true;
          }
        });
        this.photos.splice(index, 1);
        this.success.push(true);
      }
    }
    if (this.success.length === this.setPhotoIds.size ) {
      this.message = 'Selected photos were deleted';
    }
  }
}
