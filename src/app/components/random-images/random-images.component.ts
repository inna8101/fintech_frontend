import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpService } from '../../services/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-random-images',
  templateUrl: './random-images.component.html',
  styleUrls: ['./random-images.component.css']
})
export class RandomImagesComponent implements OnInit, OnDestroy {
  public hiddenImg = new Image();
  public mainImage = '../../../assets/img/image.jpg';
  private refreshIntervalId;

  constructor(
    private http: HttpService,
    private router: Router
  ) {}

  ngOnInit() {
    this.refreshIntervalId = setInterval(() => {
      this.http.getRandomImage()
      .subscribe(url => {
        this.hiddenImg.onload = () => {
          this.mainImage = url;
        }
        this.hiddenImg.src = url;
      });
    }, 5000);
  }
 
  ngOnDestroy(){
    clearInterval(this.refreshIntervalId);
  }

  morePhotos(): void {
    this.router.navigateByUrl('/search');
  }
}

