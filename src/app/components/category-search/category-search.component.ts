import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable, of } from 'rxjs';
import { Router,  ActivatedRoute} from '@angular/router';
import {PhotosService} from 'src/app/services/photos.service';
import { HttpService } from '../../services/http';

@Component({
  selector: 'app-category-search',
  templateUrl: './category-search.component.html',
  styleUrls: ['./category-search.component.css']
})
export class CategorySearchComponent implements OnInit {
  public categories$: Observable<string[]>;
  public selectedCategory = '';
  public photosByCategory$ = of([]);

  constructor(
    private photosService: PhotosService,
    private http: HttpService,
    private router: Router,
    private activatedRoute: ActivatedRoute
    ) { }

  ngOnInit() {
    this.categories$ = this.http.getCategories()
      .pipe(map(categories => categories.map(({name}) => name)));

    this.activatedRoute.queryParams.subscribe(
      params => {
      this.selectedCategory = params['category'];

      if (!this.selectedCategory) {
        this.photosByCategory$ = of([]);

        return;
      }

      this.searchPhotosByCategory();
    });
  }

  onChange(selectedCategory) {
    this.selectedCategory = selectedCategory;

    this.router.navigateByUrl('/search?category=' + this.selectedCategory);
}

searchPhotosByCategory() {
    this.photosByCategory$ = this.photosService.getPhotosByCategory(this.selectedCategory);
  }
}
