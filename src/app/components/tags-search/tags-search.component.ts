import { Component, OnInit } from '@angular/core';
import { Photo } from '../../interfaces/photo';
import {PhotosService} from 'src/app/services/photos.service';
import { Observable, of } from 'rxjs';
import { Router,  ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-tags-search',
  templateUrl: './tags-search.component.html',
  styleUrls: ['./tags-search.component.css']
})
export class TagsSearchComponent implements OnInit {
  public tags: string;
  public photosRequested = false;
  public photosByTags$ = of([]);

  constructor(
    private photosService: PhotosService,
    private router: Router,
    private activatedRoute: ActivatedRoute
    ) { }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe(
      params => {
      this.tags = params['tags'];
      if (!this.tags) {
        this.photosByTags$ = of([]);
        this.photosRequested = false;
        return;
      }

      this.searchPhotosByTags();
    });
  }
  onChange(tags) {
    this.tags = tags;
}

  searchPhotosByTags() {
    this.router.navigateByUrl('/tagssearch?tags=' + this.tags);
    this.photosByTags$ = this.photosService.getPhotosByTags(this.tags);
    this.photosRequested = true;
  }
}
