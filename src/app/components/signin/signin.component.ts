import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { HttpLoginService } from '../../services/http-login.service';
import { Router } from '@angular/router';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  public message = '';

  signInForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required),
  });

  constructor(
    private router: Router,
    private httpLogin: HttpLoginService
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.message = '';
    this.httpLogin.signIn(this.signInForm.value).subscribe(
      user => {
        this.router.navigateByUrl('/profile');
      },
      error => {
        this.message = error.error.text;
      });
}

isControlInvalid(controlName: string): boolean {
  const control = this.signInForm.controls[controlName];
  const result = control.invalid && control.touched;

  return result;
  }
}
