export interface Photo {
    _id: string,
    file_id: string,
    category_id: string,
    user_id: string,
    name: string,
    date: Date | string,
    description?: string,
    tags?: string [],
    isBanned?: boolean
}
